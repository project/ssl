<?php

/**
 * @file
 * SSL Assistant class.
 *
 * @autor: paul@azovsky.net
 */

/**
 * Defines an interface for SSL Assistant implementations.
 */
interface SSLAssistantInterface {
  public function getCert();
  public function getStatus($warnlimit);
  public function getValidFromDate($format);
  public function getValidToDate($format);
  public function getDaysLeft($format);
  public function getSubject();
  public function getIssuer();
  public function getSignatureType($type);
}

/**
 * Defines a default SSL Assistant implementation.
 */
class SSLAssistant implements SSLAssistantInterface {
  protected $url;
  protected $cert;
  protected $err;

  /**
   * Constructs a SSLAssistant object.
   */
  function __construct($url) {
    $this->url = $url;
    try {

      $conf = array(
        'verify_peer' => FALSE,
        'verify_peer_name' => FALSE,
        'capture_peer_cert' => TRUE,
        'allow_self_signed' => TRUE,
      );
      $get = stream_context_create(array("ssl" => $conf));
      $read = stream_socket_client("ssl://$this->url:443", $errno, $errstr, 30, STREAM_CLIENT_CONNECT, $get);

      if ($read) {
        $params = stream_context_get_params($read);
        if ($params) {
          $this->cert = openssl_x509_parse($params['options']['ssl']['peer_certificate']);
        }
      }

    }
    catch (Exception $exc) {
      $this->err = $exc->getMessage();
    }
  }

  /**
   * Implements SSLAssistantInterface::getCert().
   */
  public function getCert() {
    return isset($this->cert) ? $this->cert : FALSE;
  }

  /**
   * Implements SSLAssistantInterface::getStatus().
   */
  public function getStatus($warnlimit) {
    $left = $this->getDaysLeft();
    if ($left <= 0) {
      return 2;
    }
    else if (($left <= $warnlimit)) {
      return 1;
    }
    return 0;
  }

  /**
   * Implements SSLAssistantInterface::getValidFromDate().
   */
  public function getValidFromDate($format = 'Y.m.d') {
    return isset($this->cert['validFrom_time_t']) ? date($format, $this->cert['validFrom_time_t']) : 'na';
  }

  /**
   * Implements SSLAssistantInterface::getValidToDate().
   */
  public function getValidToDate($format = 'Y.m.d') {
    return isset($this->cert['validTo_time_t']) ? date($format, $this->cert['validTo_time_t']) : 'na';
  }

  /**
   * Implements SSLAssistantInterface::getDaysLeft().
   */
  public function getDaysLeft($format = '%R%a') {
    if (isset($this->cert['validTo_time_t'])) {
      $date1 = new DateTime();
      $date2 = new DateTime();
      $date2->setTimestamp($this->cert['validTo_time_t']);
      return $date1->diff($date2)->format($format);
    }
  }

  /**
   * Implements SSLAssistantInterface::getIssuer().
   */
  public function getIssuer() {
    if (isset($this->cert['issuer'])) {
      return implode(', ', $this->cert['issuer']) . '.';
    }
  }

  /**
   * Implements SSLAssistantInterface::getSubject().
   */
  public function getSubject() {
    if (isset($this->cert['subject']['CN'])) {
      return $this->cert['subject']['CN'];
    }
  }

  /**
   * Implements SSLAssistantInterface::getSignatureType().
   * @TODO: a warning about "RSA-SHA1".
   */
  public function getSignatureType($type = 'SN') {
    if (isset($this->cert['signatureType' . $type])) {
      return $this->cert['signatureType' . $type];
    }
  }

}
